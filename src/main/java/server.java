
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class server {

    private ServerSocket serversocket=null;
    private Boolean isRunning=false;

    public static int PORT=4424;

    public void init(){
        try {
            serversocket=new ServerSocket(PORT);
            startListenig();
        } catch (IOException ex) {
            Logger.getLogger(server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void startListenig() throws IOException {
        isRunning = true;
        while (isRunning) {
            Socket clientSocket = serversocket.accept();
            new Thread(new clientManager(clientSocket)).start();
        }
    }
}