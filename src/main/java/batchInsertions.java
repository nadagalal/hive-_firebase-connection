
import org.apache.hive.hcatalog.streaming.*;
import org.apache.hive.hcatalog.common.HCatUtil;
import java.util.ArrayList;

public class batchInsertions {
    public static void process(ArrayList<ArrayList<String>> aList ) {
        System.setProperty("HADOOP_USER_NAME", "hduser");
        HiveEndPoint endPt = new HiveEndPoint("thrift://192.168.1.109:9083", "tracking", "tripshistory" ,null);
        System.out.println(endPt);
        try {

            StreamingConnection connection = endPt.newConnection(false);


            String columnsFieldsArr[] = new String[3];

            columnsFieldsArr[0] = "trip_id";
            columnsFieldsArr[1] = "lon";
            columnsFieldsArr[2] = "lat";
            columnsFieldsArr[3] = "date";

            DelimitedInputWriter writer = new DelimitedInputWriter(columnsFieldsArr, ",", endPt);
            TransactionBatch txnBatch = connection.fetchTransactionBatch(3, writer);

            txnBatch.beginNextTransaction();
            for (int i = 0; i < aList.size(); ++i) {
                writeStream(connection, writer, txnBatch, aList.get(i));
                txnBatch.heartbeat();
            }
            writer.flush();
            txnBatch.commit();
            txnBatch.close();



        } catch (InvalidColumn invalidColumn) {
            invalidColumn.printStackTrace();
        } catch (ImpersonationFailed impersonationFailed) {
            impersonationFailed.printStackTrace();
        } catch (StreamingIOFailure streamingIOFailure) {
            streamingIOFailure.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvalidTable invalidTable) {
            invalidTable.printStackTrace();
        } catch (InvalidPartition invalidPartition) {
            invalidPartition.printStackTrace();
        } catch (ConnectionError connectionError) {
            connectionError.printStackTrace();
        } catch (PartitionCreationFailed partitionCreationFailed) {
            partitionCreationFailed.printStackTrace();
        } catch (SerializationError serializationError) {
            serializationError.printStackTrace();
        } catch (StreamingException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    private static void writeStream(StreamingConnection connection, DelimitedInputWriter writer, TransactionBatch txnBatch, ArrayList<String> values) {
        try {

            StringBuilder outputString= new StringBuilder();
           // outputString.append("insert into  tripshistory values("+values.get(0)+","+values.get(1)+","+values.get(2)+",'2018-09-08 00:09:00')");
            outputString.append(values.get(0));
            outputString.append(",");
            outputString.append(values.get(1));
            outputString.append(",");
            outputString.append(values.get(2));
            outputString.append(",");
            outputString.append(values.get(3));
            outputString.append(",");
           // "13,val13,Europe,Germany".getBytes()
            /*for (JsonNode fNode : schemaType) {
                String type = fNode.get("type").toString().replace("\"","");
                String fieldName = fNode.get("field").toString().replace("\"","");
                String value = node.get("value").get("payload").get("after").get(fieldName).toString();
                if(type.equals("string")){
                    outputString.append(value);
                }else {
                    outputString.append(value.replace("\"",""));
                }
                outputString.append(",");
            }

            outputString.deleteCharAt(outputString.length() - 1);

           ;*/
            System.out.println("output"+outputString.toString());
            txnBatch.write(outputString.toString().getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}

