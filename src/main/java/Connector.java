import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connector {
    private static Connection hiveConnection = null;
    private static Connector connector = null;
    private static String driverName = "org.apache.hive.jdbc.HiveDriver";

    private Connector(){}
    public static Connector getInstance()
    {
        if(connector==null)
            connector= new Connector();
        return connector;
    }
    public  Connection getHiveConnection() {

        if (this.hiveConnection == null) {
            this.hiveConnection = getConnection();
        }
        return hiveConnection;
    }

    public static Connection getConnection() {
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:hive2://192.168.1.109:10000/tracking", "root", "");


        } catch (SQLException e) {

            e.printStackTrace();
        }
        return connection;
    }
}
