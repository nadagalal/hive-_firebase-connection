import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

public class MyThread extends Thread{
    Map<String, Object> node ;
    Timestamp current;

    public MyThread(Map<String, Object> node) {
        this.node = node;
    }
    public MyThread(Map<String, Object> node, Timestamp current) {
        this.node = node;
        this.current=current;
    }



    public void run(){
        try {
            FirebaseConnection.getNode(this.node,this.current);

        } catch (SQLException e) {
//            e.printStackTrace();
        }
        System.out.println("thread is running...");
    }

}
