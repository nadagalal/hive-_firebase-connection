import java.sql.*;


import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.DriverManager;

public class hiveConnection {


    private static String driverName = "org.apache.hive.jdbc.HiveDriver";

    /**
     * @param args
     * @throws SQLException
     */
    public static void main(String[] args) throws SQLException {
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.exit(1);
        }
        //replace "hive" here with the name of the user the queries should run as
        Connection con = DriverManager.getConnection("jdbc:hive2://192.168.1.109:10000/tracking", "root", "");

        Statement stmt = con.createStatement();
        String tableName = "tripshistory";
        // stmt.execute("drop table if exists " + tableName);
        // stmt.execute("create table " + tableName + " (key int, value string)");
        // show tables
        String sql = "show tables '" + tableName + "'";
        System.out.println("Running: " + sql);
        ResultSet res = stmt.executeQuery(sql);
        if (res.next()) {
            System.out.println(res.getString(1));
        }
        // describe table
        sql = "describe " + tableName;
        System.out.println("Running: " + sql);
        res = stmt.executeQuery(sql);
        while (res.next()) {
            System.out.println(res.getString(1) + "\t" + res.getString(2));
        }

        sql = "select * from " + tableName;
        System.out.println("Running: " + sql);
        res = stmt.executeQuery(sql);
        while (res.next()) {
            System.out.println(String.valueOf(res.getInt(1)) + "\t" + res.getString(2)+ "\t" + res.getString(3)+ "\t" + res.getString(4));
        }

        sql = "insert into " + tableName + " " + "values(4,23882,224,'2018-09-08 00:09:00')";
        System.out.println("Running: " + sql);
        res = stmt.executeQuery(sql);
       /* while (res.next()) {
            System.out.println(String.valueOf(res.getInt(1)) + "\t" + res.getString(2)+ "\t" + res.getString(3)+ "\t" + res.getString(4));
        }*/
    }
}