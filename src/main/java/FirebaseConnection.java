


import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

import java.io.FileInputStream;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.google.firebase.database.*;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.snapshot.*;

public abstract class FirebaseConnection implements ValueEventListener, Runnable  {
    public static int flag = 0;
    private String threadName;



    public static void getNode(Map<String,Object> node ,Timestamp current) throws SQLException {
        int id = Integer.valueOf((String)node.get("trip_id"));
        String lon = ""+node.get("lon");
        String lat = ""+node.get("lat");
        System.out.println("id"+id+"\nlon"+lon+"\nlat"+lat);
        hiveProcess.insertHive(id,lon,lat,current);

    }
    public static void main(String args[]) throws Exception {
        //final Thread t = new Thread();
        FileInputStream refreshToken = new FileInputStream("/home/nada/Desktop/tracker-e91d2-firebase-adminsdk-50xu8-fc34b0374f.json");

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(refreshToken))
                .setDatabaseUrl("https://tracker-e91d2.firebaseio.com/")
                .build();

        FirebaseApp.initializeApp(options);

        FirebaseDatabase database = FirebaseDatabase.getInstance();

        final DatabaseReference ref = database.getReference("users");
        ref.getParent().child("users").setValue(null,null);
        ref.addValueEventListener(new ValueEventListener() {

            public void onDataChange(DataSnapshot stegoHeightSnapshot) {
                Integer favoriteDinoHeight = stegoHeightSnapshot.getValue(Integer.class);
                Query query = ref.orderByChild("9").limitToLast(2);
                query.addValueEventListener(new ValueEventListener() {


                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Data is ordered by increasing height, so we want the first entry
                        flag = 0;
                       // Date current = new Date();
                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                        DataSnapshot firstChild = dataSnapshot.getChildren().iterator().next();
                        Map<String, Object> node = (Map<String, Object>) firstChild.getValue();
                        MyThread t =new MyThread(node,timestamp);
                        t.start();


                    }


                    public void onCancelled(DatabaseError databaseError) {
                        // ...
                    }
                });
            }


            public void onCancelled(DatabaseError databaseError) {
                // ...
            }
        });
        while(true){
            Thread.sleep(1000);
        }

    }

           /* public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                firebase newPost = dataSnapshot.getValue(firebase.class);
                System.out.println("Author: " + newPost.Name);
                System.out.println("Title: " + newPost.trip_id);
                System.out.println("Previous Post ID: " + prevChildKey);
            }


            public void onChildChanged(DataSnapshot dataSnapshot, String prevChildKey) {
                System.out.println("Previous Post ID:");
            }


            public void onChildRemoved(DataSnapshot dataSnapshot) {
                System.out.println("Previous Post ID:");
            }


            public void onChildMoved(DataSnapshot dataSnapshot, String prevChildKey) {
                System.out.println("Previous Post ID:");
            }


            public void onCancelled(DatabaseError databaseError) {
                System.out.println("Previous Post ID:");
            }
        });*/
}






        /*DataSnapshot dataSnapshot = ref.addChildEventListener(new ChildEventListener());
        firebase user = dataSnapshot.getValue(firebase.class);
        System.out.println(user);
*/




// DatabaseReference myRef = database.getReference("users").child("1");

// DatabaseReference shot = myRef;

// System.out.println((shot));
        /*DatabaseReference ref = database.getReference("users");
        DataSnapshot s =null;

        String message = (String) s.child("users").getValue();
        System.out.println((message));*/
       /* ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot messageSnapshot: dataSnapshot.getChildren()) {
                    String name = (String) messageSnapshot.child("name").getValue();
                    String message = (String) messageSnapshot.child("message").getValue();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) { }
        });*/
// myRef.setValue("Hello, World!");