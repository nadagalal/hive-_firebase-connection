import java.util.ArrayList;

public class createBatch {
    public static ArrayList<ArrayList<String>> aList =
            new ArrayList<ArrayList<String> >(4);

    public static void createBatches (String id,String lon,String lat,String date){


        // Create n lists one by one and append to the
        // master list (ArrayList of ArrayList)
        ArrayList<String> list1 = new ArrayList<String>();
        list1.add(id);
        list1.add(lon);
        list1.add(lat);
        list1.add(date);
        aList.add(list1);
        if (aList.size() == 3)
        {
            batchInsertions.process(aList);
            aList = new ArrayList<ArrayList<String>>();
        }
    }


}
